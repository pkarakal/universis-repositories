# Create a Repository for Fedora packages

## General
*  Use CentOS 8, Fedora or RHEL as host OS
*  Install createrepo and httpd or nginx
    `dnf install createrepo httpd|nginx`
*  Create a directory for the packages /repository
*  Move rpms to /repository
    `cp /path/to/files /repository/`
*  Change the ownership of the directory
    `chown -R root.root /repository`
*  Create a repository out of `repository`. This will add the necessary configuration and repository metadata in an xml file.
    ```bash
    createrepo /repository/
    chmod -R o-w+r /repository
    ```
    Make sure to use the full path during `createrepo` as some versions of RHEL may configure the repository wrong if relative
    directory path is use
*  Create a repository configuration file for yum/dnf: `nano /etc/yum.repos.d/customrepo.repo` containing below configuration
    ```bash 
    [repo-id] # eg myrepo
    name=My custom repository name
    baseurl=file:///repository/
    enabled=1
    gpgcheck=0 # if the rpms are signed change it to 1 and enable the following line
    # gpgkey=file:///path/to/gpg/key
    ```
*  If everything was successful and you have added an rpm to `/repository`, you should be able to install it on your machine 
    e.g. You have a package named `my-package-1.0.0-1.fc32.x86_64.rpm` you should be able to run
    ```bash
    dnf install my-package
    ```

## Apache 
*  Create a symlink between the root and the document root
    `ln -s /repository /var/www/html/repo`
*  Start the httpd service
    `service httpd start` 
*  If everything was successful, you will be able to see it on the browser under `http:localhost/repo`


## NGINX
*  Create a symlink between the root and the document root
    `ln -s /repository /var/www/html/repo`
*  Configure NGINX `nano /etc/nginx/nginx.conf`. Edit the default config by finding the server version and making sure it contains the following configuration
    ```bash
    server {
            listen       80 default_server;
            listen       [::]:80 default_server;
            server_name  _;
            root         /var/www/html/;

            # Load configuration files for the default server block.
            include /etc/nginx/default.d/*.conf;

            location / {
                    allow all;
                    sendfile on;
                    sendfile_max_chunk 1m;
                    autoindex on;
                    autoindex_exact_size off;
                    autoindex_format html;
                    autoindex_localtime on;
            }
    }
    ```
*  Start the nginx service
    `service nginx start`
*  If everything was successful, you will be able to see it on the browser under `http:localhost/repo`


# Configure Clients
*  Create a repository configuration file for yum/dnf: `nano /etc/yum.repos.d/customrepo.repo` containing below configuration
    ```bash 
    [repo-id] # eg myrepo
    name=My custom repository name
    baseurl=http://IP_ADDRESS/repo
    enabled=1
    gpgcheck=0 # if the rpms are signed change it to 1 and enable the following line
    # gpgkey=http://IP_ADDRESS/path/to/gpg/key
    ```
*  Install a package you want
    e.g. You have a package named `my-package-1.0.0-1.fc32.x86_64.rpm` you should be able to run
    ```bash
    dnf install my-package
    ```
