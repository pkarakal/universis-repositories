# Universis Linux Repositories
This is an experimental project to try and create a common place for serving all executable programs of Universis for all major Linux distributions including Debian-based, RHEL/Fedora-based and SLES-based.

This includes a Dockerfile that when built, exposes a CentOS based NGINX server which includes all `deb`s and `rpm`s. With the correct configuration any user that has access to the server can install the packages it exposes simply by configuring the package manager of his/her distro.

To configure the clients follow the instructions below
*  Debian-based

    ```
    echo "deb http://IP_ADDRESS/repo/ubuntu/ main Release" >> /etc/sources/list.d/universis.list
    ```
*  RHEL/Fedora-based
    
    ```
    cat << EOF > /etc/yum.repos.d/universis.repo
    [repo-id] # eg myrepo
    name=My custom repository name
    baseurl=http://IP_ADDRESS/repo/fedora
    enabled=1
    gpgcheck=0 # if the rpms are signed change it to 1 and enable the following line
    # gpgkey=http://IP_ADDRESS/path/to/gpg/key
    EOF
    ```
*  SLES-based 
    
    ```
    cat <<EOF > /etc/zypp/repos.d/universis.repo
    [repo-id] # eg myrepo
    name=My custom repository name
    baseurl=http://IP_ADDRESS/repo/opensuse
    enabled=1
    path=/
    type=rpm-md
    keeppackages=0
    EOF
    ```

And then just install the package you want.

For more information on creating standalone repositories for the various distros I have also included mds with instructions.

*  [Debian-based](./create_apt_repo.md)
*  [RHEL/Fedora-based](./create_dnf_repo.md)
*  [SLES-based](./create_zypper_repo.md)

For more info you can check out the [create_repos](./create_repos.md) file and the [nginx configuration](nginx.conf). 

Note: There is also a gpg key included in the repository which uses RSA 4096 encryption and is a dummy gpg key for demonstration puproses.

## Deploy
### Prerequisites
* Docker
* Podman
* Any other OCI container tool

### Instructions
To use the Dockerfile you have to first build it using
```
docker/podman build --tag universis-repositories .
```

and then just start the container using
```
docker/podman run -d -p 8080:80 universis-repositories
```
