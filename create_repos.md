# Create a Repository for RPM packages
The following instructions will create you a server to host both Fedora and
openSUSE packages/

## General
*  Use CentOS 8, Fedora or RHEL as host OS (preferably Centos 8)
*  Enable EPEL repository 
    `dnf install epel-release -y`
*  Install createrepo, dpkg-dev, dpkg-devel and httpd or nginx
    `dnf install createrepo dpkg-dev dpkg-devel httpd|nginx `
*  Create a directory for the packages `mkdir -p /repository/`
*  Create subdirectories for fedora, opensuse and ubuntu 
    `mkdir -p /repository/fedora && mkdir -p /repository/opensuse && mkdir -p /repository/ubuntu`
*  Move rpms to /repository
    `cp /path/to/files /repository/`
*  Change the ownership of the directory
    `chown -R root.root /repository`
*  Create a repository out of `repository/ubuntu` and create a Release file
    ```
    dpkg-scanpackages -m . > Packages
    cat Packages | gzip -9c > Packages.gz
    PKGS=$(wc -c Packages)
    PKGS_GZ=$(wc -c Packages.gz)
    cat <<EOF > Release
    Architectures: amd64
    Date: $(date -R)
    MD5Sum:
    $(md5sum Packages  | cut -d" " -f1) $PKGS
    $(md5sum Packages.gz  | cut -d" " -f1) $PKGS_GZ
    SHA1:
    $(sha1sum Packages  | cut -d" " -f1) $PKGS
    $(sha1sum Packages.gz  | cut -d" " -f1) $PKGS_GZ
    SHA256:
    $(sha256sum Packages | cut -d" " -f1) $PKGS
    $(sha256sum Packages.gz | cut -d" " -f1) $PKGS_GZ
    EOF
    gpg -abs -o Release.gpg Release
    ```
*  Create a source.list file for `repository/ubuntu/` and add the following
    ```
    deb http://IP_ADDRESS/repo/ubuntu/ main Release
    ```
*  Create a repository out of `repository` subdirectories (for fedora and opensuse). This will add the necessary configuration and repository 
    metadata in an xml file.
    ```bash
    createrepo /repository/fedora
    createrepo /repository/opensuse
    chmod -R o-w+r /repository
    ```
    Make sure to use the full path during `createrepo` as some versions of RHEL may configure the repository wrong if relative
    directory path is use
*  (Optional) Create a repository configuration file for yum/dnf: `nano /etc/yum.repos.d/customrepo.repo` containing below configuration
    ```bash 
    [repo-id] # eg myrepo
    name=My custom repository name
    baseurl=file:///repository/
    enabled=1
    gpgcheck=0 # if the rpms are signed change it to 1 and enable the following line
    # gpgkey=file:///path/to/gpg/key
    ```
*  (Optional) Create a repository configuration file for zypper: `nano /etc/zypp/repos.d/customrepo.repo` containing below configuration
    ```bash 
    [repo-id] # eg myrepo
    name=My custom repository name
    baseurl=file:///repository/
    enabled=1
    path=/
    type=rpm-md
    keeppackages=0
    ```
*  (Optional) If everything was successful and you have added an rpm to `/repository`, you should be able to install it on your machine 
    e.g. You have a package named `my-package-1.0.0-1.fc32.x86_64.rpm` you should be able to run
    ```bash
    dnf/zypper install my-package
    ```

## Apache 
*  Create a symlink between the root and the document root
    `ln -s /repository /var/www/html/repo`
*  Start the httpd service
    `service httpd start` 
*  If everything was successful, you will be able to see it on the browser under `http:localhost/repo`


## NGINX
*  Create a symlink between the root and the document root
    `ln -s /repository /var/www/html/repo`
*  Configure NGINX `nano /etc/nginx/nginx.conf`. Edit the default config by finding the server version and making sure it contains the following configuration
    ```bash
    server {
            listen       80 default_server;
            listen       [::]:80 default_server;
            server_name  _;
            root         /var/www/html/;

            # Load configuration files for the default server block.
            include /etc/nginx/default.d/*.conf;

            location / {
                    allow all;
                    sendfile on;
                    sendfile_max_chunk 1m;
                    autoindex on;
                    autoindex_exact_size off;
                    autoindex_format html;
                    autoindex_localtime on;
            }
    }
    ```
*  Start the nginx service
    `service nginx start`
*  If everything was successful, you will be able to see it on the browser under `http:localhost/repo`


# Configure Clients
## Ubuntu
*  Import the public key into apt and add the repo to the sources.
    ```bash
    sudo wget --quiet http://IP_ADDRESS/repo/ubuntu/name.gpg -O /etc/apt/trusted.gpg.d/name.gpg
    sudo apt-key add /etc/apt/trusted.gpg.d/name.gpg
    sudo wget --quiet http://IP_ADDRESS/repo/ubuntu/name.source.list -O /etc/apt/sources.list.d/name.source.list
    ```
## Fedora
*  Create a repository configuration file for yum/dnf: `nano /etc/yum.repos.d/customrepo.repo` containing below configuration
    ```bash 
    [repo-id] # eg myrepo
    name=My custom repository name
    baseurl=http://IP_ADDRESS/repo/fedora
    enabled=1
    gpgcheck=0 # if the rpms are signed change it to 1 and enable the following line
    # gpgkey=http://IP_ADDRESS/path/to/gpg/key
    ```
*  Install a package you want
    e.g. You have a package named `my-package-1.0.0-1.fc32.x86_64.rpm` you should be able to run
    ```bash
    dnf install my-package
    ```

## openSUSE
*  Create a repository configuration file for yum/dnf: `nano /etc/zypp/repos.d/customrepo.repo` containing below configuration
    ```bash 
    [repo-id] # eg myrepo
    name=My custom repository name
    baseurl=http://IP_ADDRESS/repo/opensuse
    enabled=1
    path=/
    type=rpm-md
    keeppackages=0
    ```
*  Install a package you want
    e.g. You have a package named `my-package-1.0.0-1.x86_64.rpm` you should be able to run
    ```bash
    zypper in my-package
    ```
