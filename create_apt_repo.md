# Create a Repository for Fedora packages

## General
*  Use Ubuntu as host OS
*  Install dpkg-dev dpkg-sig and httpd or nginx
    `apt-get install dpkg-dev dpkg-sig apache2|nginx`
*  Create the repository in /var/www
    `mkdir -p /var/www/repo/dists/stable/main/binary`
*  Move debs to /var/www/repo/dists/stable/main/binary
    `cp /path/to/files /var/www/repo/dists/stable/main/binary/`
*  Create a gpg key to sign the deb files
    `gpg --gen-key`
*  Export your public key that was generated to a text file and store it in the root of the repository
    `gpg --output keyFile --armor --export 041DA354`
*  Sign the packages 
    ```
    dpkg-sig --sign builder file1.deb
    dpkg-sig --sign builder file2.deb
    ```
*  Change the ownership of the directory
    `chown -R root.root /var/www/repo`
*  Create an index file for the repository called Packages in the same directory as the deb files and zip it. An uncompressed Packages file must be kept there too. 
    ```bash
    cd /var/www/repo/dists/stable/main/binary
    apt-ftparchive packages . > Packages
    gzip -c Packages > Packages.gz
    ```
*  Create a Release, InRelease, and Release.gpg file:
    ```bash
    cd /var/www/repo/dists/stable/main/binary
    apt-ftparchive release . > Release
    gpg --clearsign -o InRelease Release
    gpg -abs -o Release.gpg Release
    ```


## Apache 
*  Create a symlink between the root and the document root
    `ln -s /var/www/repo /var/www/html/repo`
*  Start the httpd service
    `service httpd start`
*  If everything was successful, you will be able to see it on the browser under `http:localhost/repo`


## NGINX
*  Create a symlink between the root and the document root
    `ln -s /var/www/repo /var/www/html/repo`
*  Configure NGINX `nano /etc/nginx/nginx.conf`. Edit the default config by finding the server version and making sure it contains the following configuration
    ```bash
    server {
            listen       80 default_server;
            listen       [::]:80 default_server;
            server_name  _;
            root         /var/www/html/;

            # Load configuration files for the default server block.
            include /etc/nginx/default.d/*.conf;

            location / {
                    allow all;
                    sendfile on;
                    sendfile_max_chunk 1m;
                    autoindex on;
                    autoindex_exact_size off;
                    autoindex_format html;
                    autoindex_localtime on;
            }
    }
    ```
*  Start the nginx service
    `service nginx start`
*  If everything was successful, you will be able to see it on the browser under `http:localhost/repo`


# Configure Clients
*  Edit apt sources.list `nano /etc/apt/sources.list` and include the below configuration
    ```bash 
      deb http://IP_ADDRESS/repo/dists/stable/main/binary /
    ```
*  Update the packages list
    `apt-get update`

*  Install a package you want
    e.g. You have a package named `my-package-1.0.0-1.deb` you should be able to run
    ```bash
    apt-get install my-package
    ```
