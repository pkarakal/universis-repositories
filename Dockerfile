FROM docker.io/library/centos:latest

RUN yum upgrade -y && yum install createrepo nginx dnf-plugins-core epel-release -y && \
  dnf config-manager --set-enabled powertools && dnf install -y dpkg-dev dpkg-devel 

RUN mkdir -p /repository/fedora && mkdir -p /repository/opensuse && mkdir -p /repository/ubuntu

WORKDIR /repository/fedora
COPY ./fedora /repository/fedora
WORKDIR /repository/opensuse
COPY ./opensuse /repository/opensuse
WORKDIR /repository/ubuntu
COPY ./ubuntu /repository/ubuntu
WORKDIR /
COPY private.key .
RUN mkdir -p /var/www/html/ && chown -R root.root /repository && createrepo /repository/fedora && \
    createrepo /repository/opensuse && dpkg-scanpackages -m /repository/ubuntu > Packages && \
    cat Packages | gzip -9c > Packages.gz && PKGS=$(wc -c Packages) && PKGS_GZ=$(wc -c Packages.gz) && \
    echo -e $'Architectures: amd64\nDate: $(date -R)\nMD5Sum:\n'$(md5sum Packages  | cut -d" " -f1) $PKGS'\n'\
    $(md5sum Packages.gz  | cut -d" " -f1) $PKGS_GZ'\nSHA1:\n'$(sha1sum Packages  | cut -d" " -f1) $PKGS'\n'\
    $(sha1sum Packages.gz  | cut -d" " -f1) $PKGS_GZ'\nSHA256:\n'$(sha256sum Packages | cut -d" " -f1) $PKGS'\n'\
    $(sha256sum Packages.gz | cut -d" " -f1) $PKGS_GZ > Release && gpg --import private.key && \
    gpg -abs -o Release.gpg Release && chmod -R o-w+r /repository && ln -s /repository /var/www/html/repo
WORKDIR /etc/nginx
COPY ./nginx.conf /etc/nginx/

EXPOSE 80

CMD ["/usr/sbin/nginx", "-g", "daemon off;"]
